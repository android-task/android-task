package com.example.bookstore

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class SimpleAdapter(
    private val items: ArrayList<SwipeModel>, var clickListener: OnRowItemClickListener
) : RecyclerView.Adapter<SimpleAdapter.VH>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH {
        return VH(parent)
    }

    override fun onBindViewHolder(holder: VH, position: Int) {
        holder.bind(items[position])
        holder.initialize(items[position], clickListener)
        Log.v("testing", " holder.bind position = $position")
        Log.v("testing", " initialize position = $position")
    }

    override fun getItemCount(): Int {
        return items.size
    }

    fun removeAt(position: Int) {
        items.removeAt(position)
        notifyItemRemoved(items.indexOf(SwipeModel()))
        Log.v("testing", "simple adapter position = $position")
    }

    class VH(parent: ViewGroup) : RecyclerView.ViewHolder(
        LayoutInflater.from(parent.context).inflate(R.layout.custom_list, parent, false)) {

        val titleText = itemView.findViewById(R.id.bookListTitle) as TextView
        val imageView = itemView.findViewById(R.id.bookListImg) as ImageView
        val subtitleText = itemView.findViewById(R.id.bookListAuthor) as TextView

        fun bind(model: SwipeModel) = with(itemView) {
            titleText.text = model.getNames()
            subtitleText.text = model.getAuthors()
            imageView.setImageBitmap(model.getImage_drawables())

        }

        // Define the click listener
        fun initialize(listItem: SwipeModel, action: OnRowItemClickListener)
        {
            titleText.text = listItem.getNames()
            subtitleText.text = listItem.getAuthors()
            imageView.setImageBitmap(listItem.getImage_drawables())

            itemView.setOnClickListener {
                action.onItemClick(listItem, adapterPosition)
            }
        }
    }
}
interface OnRowItemClickListener
{
    fun onItemClick(listItem: SwipeModel, position: Int)
}
