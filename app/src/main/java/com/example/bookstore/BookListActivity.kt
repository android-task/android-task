package com.example.bookstore

import android.R.attr
import android.app.Activity
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.graphics.drawable.toDrawable
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.bookstore.databinding.ActivityBooklistBinding


const val LAUNCH_BOOKDETAILS = 1111
const val LAUNCH_CREATEBOOK = 10001

class BookListActivity : AppCompatActivity(), OnRowItemClickListener {

    private lateinit var binding: ActivityBooklistBinding
    private lateinit var viewModel: SwipeModel
    var modelArrayList = ArrayList<SwipeModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityBooklistBinding.inflate(layoutInflater)
        setContentView(binding.root)
        supportActionBar?.hide()

        setupRecycleView()
        setupArray()
        btnAction()

        viewModel = ViewModelProvider(this)[SwipeModel::class.java]

        val recyclerView = binding.recyclerView
        viewModel.addItem(booklist as ArrayList<Book>)
        val adapter = recyclerView.adapter as SimpleAdapter
        launchAct()
        adapter.notifyDataSetChanged()

    }

    override fun onResume()
    {
        super.onResume()
    }

    private fun setupRecycleView() {
        // Setup recycler view
        val recyclerView = binding.recyclerView
        recyclerView.addItemDecoration(DividerItemDecoration(this, DividerItemDecoration.VERTICAL))
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = SimpleAdapter(ArrayList<SwipeModel>(), this)

        val adapter = SimpleAdapter(modelArrayList, this)
        recyclerView.adapter = adapter
        launchAct()

        val swipeHandler = object : SwipeToDeleteCallback(this) {
            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                val adapter = recyclerView.adapter as SimpleAdapter

                val position : Int = viewHolder.adapterPosition
                val book = booklist[position]
                Log.v("testing", " book position = $position")

                val builder = AlertDialog.Builder(this@BookListActivity)
                builder.setTitle("Alert")
                builder.setMessage("Confirm to delete this book? position : $position, named = ${book.name}")
                builder.setPositiveButton("Yes", DialogInterface.OnClickListener { _, _ ->

                    viewModel.remove(book)
//                    viewModel.removePosition(position)
                    Log.v("testing", " vm position = $position")
                    adapter.removeAt(position)
                    Log.v("testing", " adapter position = $position")
                    adapter.notifyItemRemoved(position)
                    Log.v("testing", " notify position = $position")
                    adapter.notifyDataSetChanged()

                })
                builder.setNegativeButton("No", DialogInterface.OnClickListener { dialog, _ ->
                    dialog.dismiss()
                })
                builder.show()
                adapter.notifyDataSetChanged()
            }
        }
        val itemTouchHelper = ItemTouchHelper(swipeHandler)
        itemTouchHelper.attachToRecyclerView(recyclerView)
    }

    private fun setupArray()
    {
        // Setup array

        val mBookList = SharedModel.getInstance().bookList

        mBookList.forEach { book ->
            val swipeModel = SwipeModel()
            book.id?.let { swipeModel.setSwipeModel(it, book.name.toString(), book.author.toString(), book.description.toString()) }
            book.image?.let { swipeModel.setImage_drawables(it) }
            modelArrayList.add(swipeModel)
        }

    }

    private fun launchAct(){
        val returnIntent = Intent()
        val bookID = intent.getIntExtra("passedInBookId", -1)
        returnIntent.putExtra("passedInBookId", getBookFromID(bookID)?.id)
        setResult(RESULT_OK, returnIntent)

    }

    private fun btnAction()
    {
        binding.logoutBtn.setOnClickListener() {
            val intent = Intent(this, MainActivity::class.java).apply {
                addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            }.also {
                startActivity(Intent(applicationContext, MainActivity::class.java) )
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
            }
            finish()
        }

        binding.addBtn.setOnClickListener(){
            val intent = Intent(applicationContext, CreateNewBookActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
            startActivityForResult(intent, LAUNCH_CREATEBOOK)
            finish()
        }
    }

    private fun getBookFromID(bookID: Int): Book? {
        for (book in SharedModel.getInstance().bookList) {
            if (book.id == bookID) {
                return book
            }

        }
        return null

    }


    //new
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == LAUNCH_BOOKDETAILS && resultCode == Activity.RESULT_OK) {
           launchAct()
        }

        if (requestCode === LAUNCH_CREATEBOOK) {
            if (resultCode === RESULT_OK) {

            }
            if (resultCode === RESULT_CANCELED) {
                // Write your code if there's no result
            }
        }
    }

    override fun onItemClick(listItem:SwipeModel, position: Int)
    {
        Toast.makeText(this, "Click on item at $position", Toast.LENGTH_SHORT).show()

        // Prepare intent for edit book
        val intent = Intent(this, EditBookDetailsActivity::class.java)
        val id = listItem.id.toString()
        val name = listItem.name.toString()
        val author = listItem.author.toString()
        val des = listItem.description.toString()
        val bitmap = (listItem.image_drawable.toDrawable(resources)).bitmap

        intent.putExtra("passedInBookName", name)
        intent.putExtra("passedInBookAuthor", author)
        intent.putExtra("passedInBookDesc", des)
        intent.putExtra("passedInBookImg", bitmap)
        intent.putExtra("passedInPosition", "$position")

        Log.v("testing", " out position = $position")

        getBookFromID(id.toInt())
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
        startActivityForResult(intent, LAUNCH_BOOKDETAILS)
    }

}
