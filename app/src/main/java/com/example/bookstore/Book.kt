package com.example.bookstore

import android.graphics.Bitmap
import android.text.Editable

var booklist = SharedModel.getInstance().bookList
var isEdit: Boolean = false
const val GALLERY_REQUEST_CODE = 100
const val CAMERA_REQUEST_CODE = 101

data class Book(
    var id: Int? = booklist.size,
    var name: Editable,
    var description: Editable,
    var author: Editable,
    var image: Bitmap ? = null
)
