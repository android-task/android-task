package com.example.bookstore

import android.graphics.Bitmap
import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class SwipeModel: ViewModel() {
    var liveData = MutableLiveData<ArrayList<Book>> () // booklist
    var newlist = arrayListOf<SwipeModel>()

    var id: Int? = booklist.size
    var name: String? = null
    var author: String? = null
    var description: String? = null
    lateinit var image_drawable: Bitmap


    fun removePosition(position: Int)
    {
       liveData.value?.forEachIndexed { index, book ->
           if (index == position)
               liveData.value?.remove(book)
       }
    }

    fun remove(book: Book) {
            liveData.value?.remove(book)
    }

    fun getImage_drawables(): Bitmap {
        return image_drawable
    }

    fun setImage_drawables(image_drawable: Bitmap) {
        this.image_drawable = image_drawable
    }

    fun getNames(): String {
        return name.toString()
    }

    fun getAuthors(): String {
        return author.toString()
    }

    fun getDesc(): String {
        return description.toString()
    }

    fun setSwipeModel(id: Int, name: String, author: String, description: String)
    {
        this.id = id
        this.name = name
        this.author = author
        this.description = description
    }

    fun addItem(bookList: ArrayList<Book>) {

        liveData.postValue(bookList)
    }
}