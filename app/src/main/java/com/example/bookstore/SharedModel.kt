package com.example.bookstore




class SharedModel
{
    var bookList = mutableListOf<Book>()

    companion object
    {
        private var instance: SharedModel? = null
        @kotlin.jvm.JvmStatic
        fun getInstance(): SharedModel
        {
            if (instance == null)
            {
                synchronized(SharedModel::class.java) {
                    if (instance == null)
                    {
                        instance = SharedModel()
                    }
                }
            }
            return instance!!
        }

        @kotlin.jvm.JvmStatic
        fun resetInstance()
        {
            instance = null
        }
    }


}