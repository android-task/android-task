package com.example.bookstore

import android.Manifest
import android.R.attr
import android.app.Activity
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.provider.Settings
import android.util.Log
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.graphics.drawable.toBitmap
import androidx.core.graphics.drawable.toDrawable
import com.example.bookstore.databinding.ActivityBookdetailsBinding
import com.example.bookstore.databinding.ActivityBooklistBinding
import java.util.*


const val LAUNCH_BOOKLIST = 1001

class CreateNewBookActivity : AppCompatActivity() {

    private lateinit var binding: ActivityBookdetailsBinding

    var option: String = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityBookdetailsBinding.inflate(layoutInflater)
        setContentView(binding.root)

        supportActionBar?.hide()

        bookListBtnAction()
        imgOnClickAction()
        launchFromBooklistAddBtn()

        val bookID = intent.getIntExtra("passedInBookId", -1)
        val book = getBookFromID(bookID)
        if (book != null && isEdit) {
            binding.bookDetailImg.setImageBitmap(book.image)
            binding.bookDetailName.setText(binding.bookDetailName.toString())
            binding.bookDetailDesc.setText(binding.bookDetailDesc.toString())

        }
    }

    override fun onResume() {
        super.onResume()

    }

    //new
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == LAUNCH_BOOKLIST && resultCode == Activity.RESULT_OK){
            val position = data?.getStringExtra("passedInPosition")
            val intent = Intent(this, BookListActivity::class.java)
            val name = binding.bookDetailName.toString()
            val author = binding.bookDetailAuthor.toString()
            val des = binding.bookDetailDesc.toString()
            val bitmap = binding.bookDetailImg.drawable to resources

            intent.putExtra("passedInBookName", name)
            intent.putExtra("passedInBookAuthor", author)
            intent.putExtra("passedInBookDesc", des)
            intent.putExtra("passedInBookImg", bitmap)
            intent.putExtra("passedInPosition", "$position")

            Log.v("testing", " data position = $data")

            SwipeModel().addItem(booklist as ArrayList<Book>)

        }
    }

    private fun bookListBtnAction() {

        //Go back to previous page
        binding.backBtn.setOnClickListener {
            onBackPressed()
        }

        //Save book info after edit
        binding.doneBtn.setOnClickListener {
            val isBookName: Boolean = (binding.bookDetailName.text.toString().isNotEmpty() && !(binding.bookDetailName.text.toString().startsWith(" ")))
            val isAuthor: Boolean = (binding.bookDetailAuthor.text.toString().isNotEmpty() && !(binding.bookDetailAuthor.text.toString().startsWith(" ")))
            val isDesc: Boolean = (binding.bookDetailDesc.text.toString().isNotEmpty() && !(binding.bookDetailDesc.text.toString().startsWith(" ")))
            val isImg: Boolean = (binding.bookDetailImg.drawable != null)

            if (isBookName && isAuthor && isDesc && isImg) {
                this.createBook()
                val bookID = intent.getIntExtra("passedInBookId", -1)
                val intent = Intent(applicationContext, BookListActivity::class.java)
                intent.putExtra("passedInBookId", getBookFromID(bookID)?.id)
                startActivityForResult(intent, LAUNCH_BOOKLIST)
                finish()
            }else if (!isBookName && !isAuthor && !isDesc && !isImg){
                Toast.makeText(this, getString(R.string.AllNull), Toast.LENGTH_LONG).show()
            }else if (!isBookName){
                Toast.makeText(this, getString(R.string.BookNameNull), Toast.LENGTH_LONG).show()
            }else if (!isAuthor){
                Toast.makeText(this, getString(R.string.AuthorNull), Toast.LENGTH_LONG).show()
            }else if (!isDesc){
                Toast.makeText(this, getString(R.string.DescNull), Toast.LENGTH_LONG).show()
            }else if (!isImg){
                Toast.makeText(this, getString(R.string.ImgNull), Toast.LENGTH_LONG).show()
            }
        }

    }

    private fun generateUUID(): String {
        return UUID.randomUUID().toString()
    }

    private fun createBook() {
        val book = Book(
            id = this.generateUUID().length,
            name = binding.bookDetailName.text,
            description = binding.bookDetailDesc.text,
            author = binding.bookDetailAuthor.text,
            image = binding.bookDetailImg.drawable.toBitmap(width = 200, height = 200)
        )

        SharedModel.getInstance().bookList.add(book)

        finish()
    }

    private fun getBookFromID(bookID: Int): Book? {
        for (book in SharedModel.getInstance().bookList) {
//
            if (book.id == bookID) {
                return book
            }

        }
        return null

    }

    private fun imgOnClickAction() {
        binding.bookDetailImg.setOnClickListener {
            chooseImageGallery()
        }
    }

    private fun chooseImageGallery() {
        val actionList = arrayOf("Gallery", "Camera", "Cancel")
        val dialogBuilder = AlertDialog.Builder(this)
        dialogBuilder.setTitle("Choose action:")
        dialogBuilder.setCancelable(false)
        dialogBuilder.setItems(actionList) { dialog, item ->
            if (item == 0) {
                if (isGalleryPermissionsAllowed()) {
                    option = actionList[item]
                    openGalleryForImage()
                } else {
                    askForGalleryPermissions()
                }
            } else if (item == 1) {
                if (isCameraPermissionsAllowed()) {
                    option = actionList[item]
                    openCamera()
                } else {
                    askForCameraPermissions()
                }

            } else {
                dialog.dismiss()
            }
        }
        dialogBuilder.show()
    }

    private fun openGalleryForImage() {
        val intent = Intent(Intent.ACTION_PICK)
        intent.type = "image/*"
        resultLauncher.launch(intent)
    }

    private fun openCamera() {
        val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        resultLauncher.launch(cameraIntent)
    }

    private var resultLauncher =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            val data: Intent? = result.data
            if (result.resultCode == Activity.RESULT_OK && option == "Gallery") {
                binding.bookDetailImg.setImageURI(data?.data)
            } else if (result.resultCode == Activity.RESULT_OK && option == "Camera") {
                binding.bookDetailImg.setImageBitmap(data?.extras?.get("data") as Bitmap)
            }
        }

    private fun isGalleryPermissionsAllowed(): Boolean {
        return ContextCompat.checkSelfPermission(
            this,
            Manifest.permission.READ_EXTERNAL_STORAGE
        ) == PackageManager.PERMISSION_GRANTED
    }

    private fun isCameraPermissionsAllowed(): Boolean {
        return ContextCompat.checkSelfPermission(
            this,
            Manifest.permission.CAMERA
        ) == PackageManager.PERMISSION_GRANTED
    }

    private fun askForGalleryPermissions(): Boolean {
        if (!isGalleryPermissionsAllowed()) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(
                    this as Activity,
                    Manifest.permission.READ_EXTERNAL_STORAGE
                )
            ) {
                showPermissionDeniedDialog()
            } else {
                ActivityCompat.requestPermissions(
                    this as Activity,
                    arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE),
                    GALLERY_REQUEST_CODE
                )
            }
            return false
        }
        return true
    }

    private fun askForCameraPermissions(): Boolean {
        if (!isCameraPermissionsAllowed()) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(
                    this as Activity,
                    Manifest.permission.CAMERA
                )
            ) {
                showPermissionDeniedDialog()
            } else {
                ActivityCompat.requestPermissions(
                    this as Activity,
                    arrayOf(Manifest.permission.CAMERA),
                    CAMERA_REQUEST_CODE
                )
            }
            return false
        }
        return true
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            GALLERY_REQUEST_CODE -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    option = "Gallery"
                    openGalleryForImage()
                } else {
                    askForGalleryPermissions()
                }
                return
            }
            CAMERA_REQUEST_CODE -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    option = "Camera"
                    openCamera()
                } else {
                    askForCameraPermissions()
                }
                return
            }
        }
    }

    private fun showPermissionDeniedDialog() {
        AlertDialog.Builder(this)
            .setTitle("Permission Denied")
            .setMessage("Permission is denied, Please allow permissions from App Settings.")
            .setPositiveButton("App Settings",
                DialogInterface.OnClickListener { _, _ ->
                    // send to app settings if permission is denied permanently
                    val intent = Intent()
                    intent.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
                    val uri = Uri.fromParts("package", packageName, null)
                    intent.data = uri
                    startActivity(intent)
                })
            .setNegativeButton("Cancel", null)
            .show()
    }

    private fun launchFromBooklistAddBtn(){

        val returnIntent = Intent()
        setResult(Activity.RESULT_CANCELED, returnIntent)

    }
}