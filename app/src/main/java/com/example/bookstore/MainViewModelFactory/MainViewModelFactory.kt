package com.example.bookstore.MainViewModelFactory

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.bookstore.SwipeModel

class MainViewModelFactory(): ViewModelProvider.Factory{
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if(modelClass.isAssignableFrom(SwipeModel::class.java)){
            return SwipeModel() as T
        }
        throw IllegalArgumentException ("UnknownViewModel")
    }

}