package com.example.bookstore

import android.Manifest
import android.app.Activity
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.provider.Settings
import android.util.Log
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.graphics.drawable.toBitmap
import com.example.bookstore.databinding.ActivityBookdetailsBinding
import java.util.*


class EditBookDetailsActivity : AppCompatActivity() {
    private lateinit var binding: ActivityBookdetailsBinding
    var option: String = ""
    var position: Int = -1
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityBookdetailsBinding.inflate(layoutInflater)
        setContentView(binding.root)
        supportActionBar?.hide()

        bookDetailsBtnAction()
        imgOnClickAction()

    }

    override fun onResume() {
        super.onResume()
        launchAct()
        editBook()
    }

    private fun editBook() {
        val book = Book(
//            id = bookID,
            name = binding.bookDetailName.text,
            description = binding.bookDetailDesc.text,
            author = binding.bookDetailAuthor.text,
            image = binding.bookDetailImg.drawable.toBitmap(width = 200, height = 200)
        )

        booklist[position] = book

    }

    private fun getBookFromID(bookID: Int): Book? {
        for (book in SharedModel.getInstance().bookList) {
            if (book.id == bookID) {
                return book
            }

        }
        return null

    }

    private fun launchAct(){
        val returnIntent = Intent()
        val bookID = intent.getIntExtra("passedInBookId", -1)
        returnIntent.putExtra("passedInBookId", getBookFromID(bookID)?.id)
        val bName = intent.getStringExtra("passedInBookName")
        val bAuthor = intent.getStringExtra("passedInBookAuthor")
        val bDesc = intent.getStringExtra("passedInBookDesc")
        val bImg = intent.getParcelableExtra<Bitmap>("passedInBookImg")

        position = intent.getStringExtra("passedInPosition")?.toInt() ?: 0
        Log.v("testing", "get position $position")

        binding.bookDetailName.setText(bName)
        binding.bookDetailAuthor.setText(bAuthor)
        binding.bookDetailDesc.setText(bDesc)
        binding.bookDetailImg.setImageBitmap(bImg)

        if (intent != null) {
            binding.doneBtn.text = "Edit"
            binding.bookDetailName.isEnabled = false
            binding.bookDetailDesc.isEnabled = false
            binding.bookDetailAuthor.isEnabled = false
            binding.bookDetailImg.isEnabled = false

        }
        setResult(RESULT_OK, returnIntent)

    }

    private fun bookDetailsBtnAction() {
        binding.backBtn.setOnClickListener() {
            onBackPressed()
        }

        binding.doneBtn.setOnClickListener() {
            if (!isEdit) {
                binding.doneBtn.text = "Done"
                binding.bookDetailName.isEnabled = true
                binding.bookDetailDesc.isEnabled = true
                binding.bookDetailAuthor.isEnabled = true
                binding.bookDetailImg.isEnabled = true
            }

            if (binding.doneBtn.text == "Done" ) {
                binding.doneBtn.setOnClickListener {
                    val isBookName: Boolean = (binding.bookDetailName.text.toString().isNotEmpty() && !(binding.bookDetailName.text.toString().startsWith(" ")))
                    val isAuthor: Boolean = (binding.bookDetailAuthor.text.toString().isNotEmpty() && !(binding.bookDetailAuthor.text.toString().startsWith(" ")))
                    val isDesc: Boolean = (binding.bookDetailDesc.text.toString().isNotEmpty() && !(binding.bookDetailDesc.text.toString().startsWith(" ")))
                    val isImg: Boolean = (binding.bookDetailImg.drawable != null)

                    if (!isBookName){
                        Toast.makeText(this, getString(R.string.BookNameNull), Toast.LENGTH_LONG).show()
                    }else if (!isAuthor){
                        Toast.makeText(this, getString(R.string.AuthorNull), Toast.LENGTH_LONG).show()
                    }else if (!isDesc){
                        Toast.makeText(this, getString(R.string.DescNull), Toast.LENGTH_LONG).show()
                    }else if (!isImg){
                        Toast.makeText(this, getString(R.string.ImgNull), Toast.LENGTH_LONG).show()
                    }else{
                        val bookID = intent.getIntExtra("passedInBookId", -1)
                        val intent = Intent(applicationContext, BookListActivity::class.java)
                        intent.putExtra("passedInBookId", getBookFromID(bookID)?.id)
                        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
                        startActivityForResult(intent, LAUNCH_BOOKLIST)
                        finish()
                    }
                }
            }

        }
    }

    private fun imgOnClickAction() {
        binding.bookDetailImg.setOnClickListener {
            chooseImageGallery()

        }
    }

    private fun chooseImageGallery() {
        val actionList = arrayOf("Gallery", "Camera", "Cancel")
        val dialogBuilder = AlertDialog.Builder(this)
        dialogBuilder.setTitle("Choose action:")
        dialogBuilder.setCancelable(false)
        dialogBuilder.setItems(actionList) { dialog, item ->
            if (item == 0) {
                if (isGalleryPermissionsAllowed()) {
                    option = actionList[item]
                    openGalleryForImage()
                } else {
                    askForGalleryPermissions()
                }
            } else if (item == 1) {
                if (isCameraPermissionsAllowed()) {
                    option = actionList[item]
                    openCamera()
                } else {
                    askForCameraPermissions()
                }

            } else {
                dialog.dismiss()
            }
        }
        dialogBuilder.show()
    }

    private fun openGalleryForImage() {
        val intent = Intent(Intent.ACTION_PICK)
        intent.type = "image/*"
        resultLauncher.launch(intent)
    }

    private fun openCamera() {
        val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        resultLauncher.launch(cameraIntent)
    }

    private var resultLauncher =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            val data: Intent? = result.data
            if (result.resultCode == Activity.RESULT_OK && option == "Gallery") {
                binding.bookDetailImg.setImageURI(data?.data)
            } else if (result.resultCode == Activity.RESULT_OK && option == "Camera") {
                binding.bookDetailImg.setImageBitmap(data?.extras?.get("data") as Bitmap)
            }
        }

    private fun isGalleryPermissionsAllowed(): Boolean {
        return ContextCompat.checkSelfPermission(
            this,
            Manifest.permission.READ_EXTERNAL_STORAGE
        ) == PackageManager.PERMISSION_GRANTED
    }

    private fun isCameraPermissionsAllowed(): Boolean {
        return ContextCompat.checkSelfPermission(
            this,
            Manifest.permission.CAMERA
        ) == PackageManager.PERMISSION_GRANTED
    }

    private fun askForGalleryPermissions(): Boolean {
        if (!isGalleryPermissionsAllowed()) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(
                    this as Activity,
                    Manifest.permission.READ_EXTERNAL_STORAGE
                )
            ) {
                showPermissionDeniedDialog()
            } else {
                ActivityCompat.requestPermissions(
                    this as Activity,
                    arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE),
                    GALLERY_REQUEST_CODE
                )
            }
            return false
        }
        return true
    }

    private fun askForCameraPermissions(): Boolean {
        if (!isCameraPermissionsAllowed()) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(
                    this as Activity,
                    Manifest.permission.CAMERA
                )
            ) {
                showPermissionDeniedDialog()
            } else {
                ActivityCompat.requestPermissions(
                    this as Activity,
                    arrayOf(Manifest.permission.CAMERA),
                    CAMERA_REQUEST_CODE
                )
            }
            return false
        }
        return true
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            GALLERY_REQUEST_CODE -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    option = "Gallery"
                    openGalleryForImage()
                } else {
                    askForGalleryPermissions()
                }
                return
            }
            CAMERA_REQUEST_CODE -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    option = "Camera"
                    openCamera()
                } else {
                    askForCameraPermissions()
                }
                return
            }
        }
    }

    private fun showPermissionDeniedDialog() {
        AlertDialog.Builder(this)
            .setTitle("Permission Denied")
            .setMessage("Permission is denied, Please allow permissions from App Settings.")
            .setPositiveButton("App Settings",
                DialogInterface.OnClickListener { _, _ ->
                    // send to app settings if permission is denied permanently
                    val intent = Intent()
                    intent.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
                    val uri = Uri.fromParts("package", packageName, null)
                    intent.data = uri
                    startActivity(intent)
                })
            .setNegativeButton("Cancel", null)
            .show()
    }

}